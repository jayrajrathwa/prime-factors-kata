package kata;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactor {
    public List<Integer> of(int number) {
        int factor = number;
        List<Integer> factors = new ArrayList<>();
        for (int divisor = 2; divisor <= factor; divisor++) {
            while (factor % divisor == 0) {
                factors.add(divisor);
                factor = factor / divisor;
            }
        }
        return factors;
    }
}
