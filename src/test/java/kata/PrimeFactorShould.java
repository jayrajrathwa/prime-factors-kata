package kata;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PrimeFactorShould {
    private PrimeFactor primeFactor;

    @BeforeEach
    void init() {
        primeFactor = new PrimeFactor();
    }

    @Test
    void return_empty_list_for_prime_factors_of_1() {
        List<Integer> primeFactors = primeFactor.of(1);
        assertTrue(primeFactors.isEmpty());
    }

    @Test
    void return_list_with_only_2_for_prime_factors_of_2() {
        List<Integer> primeFactors = primeFactor.of(2);
        assertIterableEquals(primeFactors, List.of(2));
    }

    @Test
    void return_list_with_only_3_for_prime_factors_of_3() {
        List<Integer> primeFactors = primeFactor.of(3);
        assertIterableEquals(primeFactors, List.of(3));
    }

    @Test
    void return_list_of_2_2_for_prime_factors_of_4() {
        List<Integer> primeFactors = primeFactor.of(4);
        assertIterableEquals(primeFactors, List.of(2,2));
    }

    @Test
    void return_list_with_only_5_for_prime_factors_of_5() {
        List<Integer> primeFactors = primeFactor.of(5);
        assertIterableEquals(primeFactors, List.of(5));
    }

    @Test
    void return_list_of_2_3_for_prime_factors_of_6() {
        List<Integer> primeFactors = primeFactor.of(6);
        assertIterableEquals(primeFactors, List.of(2,3));
    }

    @Test
    void return_list_with_only_7_for_prime_factors_of_7() {
        List<Integer> primeFactors = primeFactor.of(7);
        assertIterableEquals(primeFactors, List.of(7));
    }

    @Test
    void return_list_of_2_2_2_for_prime_factors_of_8() {
        List<Integer> primeFactors = primeFactor.of(8);
        assertIterableEquals(primeFactors, List.of(2,2,2));
    }

    @Test
    void return_list_of_3_3_for_prime_factors_of_9() {
        List<Integer> primeFactors = primeFactor.of(9);
        assertIterableEquals(primeFactors, List.of(3,3));
    }
}





